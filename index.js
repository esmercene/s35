const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')

dotenv.config()

const app = express();
const port = 3001

// MonogoDB Connection
mongoose.connect(`mongodb+srv://esmercene:${process.env.MONGODB_PASSWORD}@cluster0.t5xp9id.mongodb.net/?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', () => console.error("Connection error"))
db.on('open', () => console.log("Connected to MongodDB"))
// MonogoDB Connection END


 app.use(express.json())
 app.use(express.urlencoded({extended: true}))








 const userSchema = new mongoose.Schema({
     username: String,
     password: String
     
 })

 const User = mongoose.model('User', userSchema)
 

 // Routes
 app.post('/signup', (request, response) => {
     User.findOne({username: request.body.username}, (error, result) => {
         if(result != null && result.username == request.body.username){
             return response.send('Username Not Available')
         } 

         let newUser = new User({
             username: request.body.username,
             password: request.body.password
         })

         newUser.save((error, savedUser) => {
             if(error){
                 return console.error(error)
             }
             else {
                 return response.status(200).send('New User Registered!')
             }
         })
     })
 })


// app.get('/tasks', (request, response) => {
//     Task.find({}, (error, result ) => {
//         if(error ) {
//             return console.log(error)
//             }
//             return response.status(200).json({
//                 data: result
//             })    
//     })
// })


// Routes END
 
 app.listen(port, () => console.log(`Server Running at Localhost: ${port}`))












